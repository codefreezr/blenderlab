import bpy
import mido
from mido import MidiFile
from mido import MidiTrack


class Absolute_time_message:
    message = None
    time = None
    def __init__(self, message, abs_time):
        self.message = message
        self.time = abs_time


def get_all_tempos(mid):
    tempoList=[]
    for track in mid.tracks:
        for msg in track:
            if msg.is_meta:
                if msg.type == "set_tempo":
                    tempoList.append(msg.tempo)
    return tempoList



def tempos_are_same(tempoList):
    previousTempo = tempoList[0]

    for tempo in tempoList[1:]:
        if not previousTempo == tempo:
            return False
        previousTempo = tempo
    return True


def get_midi_tempo(mid):
    tempoList = get_all_tempos(mid)
    if tempos_are_same(tempoList):
        return tempoList[0]

def absolute_time_messages(midtrack, ticks_per_beat, tempo):
    ticks_before = 0
    absolute_time_events = []
    for message in midtrack:
        message_delta = message.time
        absolute_ticks = message_delta + ticks_before
        abs_time = mido.tick2second(absolute_ticks, ticks_per_beat, tempo)
        ticks_before += message.time
        absolute_time_events.append(Absolute_time_message(message, abs_time))
    return absolute_time_events


def seconds2frames(seconds):
    fps = bpy.context.scene.render.fps
    return round(fps * seconds)

class Drum:
    drum_object = None
    def __init__(self, drum_object):
        self.drum_object = drum_object

    def hit(self, hit_time_seconds):
        #current_material = self.drum_object.active_material
        current_material = bpy.data.materials["Material"]
        emission = current_material.node_tree.nodes['Emission']
        emitStr = emission.inputs[1].default_value

        start_frame = seconds2frames(hit_time_seconds)
        frame_length = 10
        brightness = 1
        brightness_dim_rate = brightness / frame_length

        for i in range(frame_length):
            current_frame = i + start_frame
            bpy.context.scene.frame_set(current_frame)

            emission.inputs[1].default_value = brightness
            print(brightness)
            current_material.node_tree.keyframe_insert(data_path='nodes["Emission"].inputs[1].default_value', frame = current_frame, index=-1)
            #self.drum_object.location.z = brightness
            #self.drum_object.keyframe_insert(data_path="location", index=-1)
            brightness -= brightness_dim_rate

obj = bpy.data.objects['Cube']

drum = Drum(obj)


mid = MidiFile("./midi-spread.mid")

midTrack = mid.tracks[8]

tempo = get_midi_tempo(mid)
absoluteTracks  = absolute_time_messages(midTrack, mid.ticks_per_beat, tempo)

for events in absoluteTracks:
    if not events.message.is_meta:
        if events.message.type == 'note_on':
            print("setting events" + str(events.time))
            drum.hit(events.time)





