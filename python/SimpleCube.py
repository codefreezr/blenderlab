import bpy

def strVector3( v3 ):
    return str(v3.x) + "," + str(v3.y) + "," + str(v3.z)

# create a new cube
bpy.ops.mesh.primitive_cube_add()

# newly created cube will be automatically selected
cube = bpy.context.selected_objects[0]
# change name
cube.name = "MyLittleCube"

# change its location
cube.location = (0.0, 5.0, 0.0)

# done
print("Done creating MyCube at position " + strVector3(cube.location) + " with name " + cube.name)