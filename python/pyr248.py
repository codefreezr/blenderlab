#!BPY

	# By Ed Montgomery (edmontgomery@hotmail.com), August 12, 2007

	# Tested with Blender 2.44 using Python 2.5

	"""

	Name: 'Pyramid_248'

	Blender: 248

	Group: 'AddMesh'

	"""
	__author__ = ["Ed Montgomery"]
	__version__ = '1.00 08/12/2007'
	__url__ = ["Script Index, http://wiki.blender.org/index.php/Scripts/Manual/Add/Pyramid", "Script, http://friendlycanadian.googlepages.com/add_mesh_pyramid.py", "Author Site , http://friendlycanadian.googlepages.com/"]
	__email__=["edmontgomery@hotmail.com"]
	
	
	__bpydoc__ = """

	

	Usage:

	

	* Launch from Add Mesh menu

	

	* Modify parameters as desired or keep defaults

	

	"""
	   #* == Pyramid maker == 

	
	    #* {{ScriptInfo

	    #* |name=Pyramid maker

	   # * |tooltip=Makes modifiable pyramid mesh.

	   # * |menu=Group: Add_Mesh

	    #* |version=1.0 August 17, 2007

	    #* |blender=2.44

	   # * |author=Ed Montgomery

	    #* |license=GPL

	    #* |link=http://friendlycanadian.googlepages.com/add_mesh_pyramid.py

	    #* }} 

	
	import BPyAddMesh
	import Blender
	
	def add_mymesh(PREF_WIDTH, PREF_LENGTH, PREF_HEIGHT):
	
		verts = []
		faces = []
		
		# define object vertices	

		verts.append([-(PREF_WIDTH/2),(PREF_LENGTH/2),0.0])
		verts.append([-(PREF_WIDTH/2),-(PREF_LENGTH/2),0.0])
		verts.append([(PREF_WIDTH/2),-(PREF_LENGTH/2),0.0])
		verts.append([(PREF_WIDTH/2),(PREF_LENGTH/2),0.0])
	        verts.append([0.0,0.0,(PREF_HEIGHT/2)])
		
		# define object faces

	        faces.append([0,1,2,3])
	        faces.append([0,1,4])
	        faces.append([1,2,4])
	        faces.append([2,3,4])
	        faces.append([3,0,4])
		
		return verts, faces
	
	def main():
		
		# Numeric input with default value of 1

		pyramidWidthInput = Blender.Draw.Create(1.0)
		
		# and same for length

		pyramidLengthInput  = Blender.Draw.Create(1.0)
	
	        # And actual height of pyramid

	        pyramidHeightInput = Blender.Draw.Create(1.0)
	
		# array for popup window's content	

		block = []
		
		# add inputs to array with title, min/max values and tooltip

		block.append(("width: ", pyramidWidthInput, 0.01, 100, "width of the pyramid"))
		block.append(("length: ", pyramidLengthInput, 0.01, 100, "length of the pyramid"))
	        block.append(("height: ", pyramidHeightInput, 0.01, 100, "height of the pyramid"))
		
		# draw	popup if it is not open allready 

		if not Blender.Draw.PupBlock("Create pyramid",block):
			return
		
	        verts, faces = add_mymesh(pyramidWidthInput.val, pyramidLengthInput.val, pyramidHeightInput.val)
		
	        BPyAddMesh.add_mesh_simple('Pyramid', verts, [], faces)
	
	main()