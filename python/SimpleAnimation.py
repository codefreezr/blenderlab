import bpy

positions = (3,3,1),(4,1,1),(3,-5,1),(-3,-5,1),(-3,5,1)
start_pos = (0,0,0)
frame_num = 0

ob = bpy.data.objects["Sphere"]
ctx = bpy.context.scene
ctx.frame_end = 100


for position in positions:
    ctx.frame_set(frame_num)
    ob.location = position
    ob.keyframe_insert(data_path="location", index =-1)
    frame_num +=20